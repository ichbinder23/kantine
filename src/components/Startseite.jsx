/* eslint-disable import/no-named-as-default */
import React from 'react';
import { Grid } from '@material-ui/core';

import Headline from './ui/Headline';
import Article from './ui/Article';
import { text } from '../constant';

import headlineImg from '../assets/Kuchen.jpg';
import essen from '../assets/essen01.jpg';
import kantine from '../assets/kantine01.jpg';

const Startseite = () => {
  return (
    <Grid container direction="column" justify="flex-start">
      <Grid item>
        <Headline
          imgSrc={headlineImg}
          headlineText="Die Kantinenwirtschaft"
          isMainPage
        />
      </Grid>
      <Grid item>
        <Article
          headline={text.article.headline}
          subHeadline={text.article.subheadline}
          text={text.article.teaserText}
          articleImg={kantine}
          teaser
        />
      </Grid>
      <Grid item>
        <Article
          headline={text.article2.headline}
          subHeadline={text.article2.subheadline}
          text={text.article2.teaserText}
          articleImg={essen}
          teaser
        />
      </Grid>
    </Grid>
  );
};

export default Startseite;
