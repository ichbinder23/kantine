import React from 'react';
import PropTypes from 'prop-types';

import { makeStyles } from '@material-ui/styles';
import { Grid, Typography } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  headerImage: {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    height: '100%',
    width: '100%'
  },
  wrapper: {
    width: '100vw',
    position: 'relative'
  },
  banner: {
    background: theme.palette.common.goldgelb,
    position: 'absolute',
    width: '90%',
    textAlign: 'center',
    padding: '4%',
    marginRight: '5%',
    marginLeft: '5%',
    opacity: '0.8'
  },
  headline: {
    ...theme.typography.headline,
    fontSize: '7vw'
  },
  subHeadline: {
    color: theme.palette.common.weinrot,
    fontWeight: 'bold',
    fontSize: '8vw'
  }
}));

const Headline = ({ imgSrc, headlineText, isMainPage }) => {
  const classes = useStyles();
  return (
    <Grid
      container
      style={{ height: isMainPage ? '70vh' : '70vh' }}
      className={classes.wrapper}
      alignItems="center"
    >
      <Grid item className={classes.banner}>
        <Typography className={classes.headline}>
          {headlineText}
        </Typography>
        {isMainPage && (
          <Typography variant="h2" className={classes.subHeadline}>
            Essen für die Seele
          </Typography>
        )}
      </Grid>
      <div
        style={{ backgroundImage: `url(${imgSrc})` }}
        className={classes.headerImage}
      />
    </Grid>
  );
};

Headline.propTypes = {
  imgSrc: PropTypes.string.isRequired,
  headlineText: PropTypes.string.isRequired,
  isMainPage: PropTypes.bool
};

Headline.defaultProps = {
  isMainPage: false
};

export default Headline;
