import React, { useState } from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import {
  AppBar,
  Toolbar,
  IconButton,
  Grid,
  Button,
  Drawer
} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import CloseIcon from '@material-ui/icons/Close';
import { Link } from 'react-router-dom';

import constant from '../../constant';

import logo from '../../assets/Logo.svg';

const userStyles = makeStyles((theme) => ({
  toolbarMargin: {
    ...theme.mixins.toolbar,
    marginBottom: '1em',
    [theme.breakpoints.down('xs')]: {
      marginBottom: '1.5em'
    }
  },
  logo: {
    height: '4.31em',
    width: '2.69em',
    marginTop: '0.5em',
    marginBottom: '0.5em',
    marginLeft: '1.38em'
  },
  menuButtonContainer: {
    marginBottom: '-0.10em',
    marginRight: '0.35em'
  },
  menuOpener: {
    height: '45px',
    width: '45px'
  },
  menuCloser: {
    height: '45px',
    width: '45px'
  },
  menuCloserContainer: {
    marginTop: '0.5em',
    marginRight: '0.55rem'
  },
  drawerMaxScreenWidth: {
    width: '100vw'
  },
  drawer: {
    background: theme.palette.common.goldgelb
  },
  menuLink: {
    ...theme.typography.link,
    width: '100vw',
    padding: 20
  }
}));

const Header = () => {
  const classes = userStyles();
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  const drawerMenu = (
    <Drawer
      anchor="right"
      open={isMenuOpen}
      onClose={() => setIsMenuOpen(!isMenuOpen)}
      classes={{ paper: classes.drawer }}
      transitionDuration={{ enter: 700, exit: 400 }}
    >
      <Grid container justify="flex-end">
        <IconButton
          edge="start"
          color="primary"
          aria-label="menu"
          className={classes.menuCloserContainer}
          onClick={() => setIsMenuOpen(false)}
        >
          <CloseIcon className={classes.menuCloser} />
        </IconButton>
      </Grid>
      <Grid
        container
        justify="center"
        alignItems="center"
        direction="column"
        wrap="nowrap"
      >
        {Object.keys(constant).map((menuKey) => (
          <Grid item key={`${menuKey}-${constant[menuKey]}`}>
            <Button
              component={Link}
              to={`/${constant[menuKey]}`}
              className={classes.menuLink}
              onClick={() => setIsMenuOpen(false)}
            >
              {constant[menuKey]}
            </Button>
          </Grid>
        ))}
      </Grid>
    </Drawer>
  );

  return (
    <>
      <AppBar position="static">
        <Toolbar disableGutters>
          <Grid container justify="space-between" alignItems="center">
            <img
              alt="header logo"
              className={classes.logo}
              src={logo}
            />
            <IconButton
              edge="start"
              color="secondary"
              aria-label="menu"
              className={classes.menuButtonContainer}
              onClick={() => setIsMenuOpen(true)}
            >
              <MenuIcon className={classes.menuOpener} />
            </IconButton>
            {drawerMenu}
          </Grid>
        </Toolbar>
      </AppBar>

      {/* <div className={classes.toolbarMargin} /> */}
    </>
  );
};

export default Header;
