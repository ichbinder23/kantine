/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Grid, Typography, Button } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  wrapper: {
    background: theme.palette.common.weinrot,
    width: '100%',
    position: 'relative',
    padding: '10% 5%'
  },
  subHeadline: {
    paddingTop: '3%',
    paddingBottom: '5%'
  },
  text: {
    ...theme.typography.text,
    textAlign: 'justify',
    marginBottom: '5%'
  },
  articleImg: {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: '100%'
  },
  imgContainer: {
    maxHeight: '40vh',
    overflow: 'hidden',
    marginTop: '6%'
  },
  button: {
    width: '60%',
    borderRadius: 0
  }
}));

const Article = ({
  headline,
  subHeadline,
  text,
  articleImg,
  teaser
}) => {
  const classes = useStyles();
  return (
    <Grid container direction="column" className={classes.wrapper}>
      <Typography variant="h1">{headline}</Typography>
      <Typography variant="h2" className={classes.subHeadline}>
        {subHeadline}
      </Typography>
      <Typography className={classes.text}>{text}</Typography>
      {teaser && (
        <Button
          variant="outlined"
          color="secondary"
          className={classes.button}
        >
          Weiter lesen {'->'}
        </Button>
      )}
      <Grid item className={classes.imgContainer}>
        {articleImg && (
          <img
            alt="article"
            src={articleImg}
            className={classes.articleImg}
          />
        )}
      </Grid>
    </Grid>
  );
};

Article.propTypes = {
  headline: PropTypes.string.isRequired,
  subHeadline: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  articleImg: PropTypes.string,
  teaser: PropTypes.bool
};

Article.defaultProps = {
  articleImg: '',
  teaser: false
};

export default Article;
