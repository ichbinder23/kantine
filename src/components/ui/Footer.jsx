import React from 'react';
import { Grid, Typography, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles((theme) => ({
  wrapper: {
    position: 'absolute'
  }
}));

const Footer = () => {
  const classes = useStyles();
  return (
    <Grid container className={classes.wrapper} direction="column">
      <Grid item>
        <Grid container justify="space-around">
          <Grid item>
            <Button color="secondary">
              <Typography variant="subtitle1">Impressum</Typography>
            </Button>
            <Button color="secondary">
              <Typography variant="subtitle1">
                Datenschutzerklärung
              </Typography>
            </Button>
          </Grid>
        </Grid>
        <Grid
          container
          justify="center"
          style={{ paddingBottom: '5%' }}
        >
          <Grid item>
            <Typography variant="subtitle1">
              COPYRIGHT © Helmut Schattka - 2014 - 2020
            </Typography>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Footer;
