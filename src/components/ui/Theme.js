import createMuiTheme from '@material-ui/core/styles/createMuiTheme';

export const kantineRot = '#601717';
export const kantineGold = '#DFC7AB';
// const kantineGrey = '#868686';

export default createMuiTheme({
  palette: {
    common: {
      weinrot: kantineRot,
      goldgelb: kantineGold
    },
    primary: {
      main: kantineRot
    },
    secondary: {
      main: kantineGold
    }
  },
  typography: {
    h1: {
      fontFamily: 'Belleza',
      fontWeight: 400,
      fontSize: '1rem',
      textTransform: 'uppercase',
      color: 'white'
    },
    h2: {
      fontFamily: 'Alex Brush',
      fontWeight: 400,
      fontSize: '2rem',
      color: kantineGold
    },
    subtitle1: {
      fontFamily: 'Belleza',
      fontWeight: 400,
      fontSize: '0.6rem',
      textTransform: 'uppercase',
      color: kantineGold
    },
    text: {
      fontFamily: 'Belleza',
      fontWeight: 400,
      fontSize: '1rem',
      color: 'white'
    },
    link: {
      fontFamily: 'Merriweather',
      fontWeight: 400,
      fontSize: '1.20rem',
      color: kantineRot
    },
    headline: {
      fontFamily: 'Merriweather',
      fontWeight: 700,
      fontSize: '1.8rem',
      color: kantineRot
    },
    footer: {
      fontFamily: 'Belleza',
      fontWeight: 400,
      fontSize: '1rem',
      color: kantineGold
    },
    learnButton: {
      borderColor: kantineRot,
      color: kantineRot,
      borderWidth: 2,
      textTransform: 'none',
      borderRadius: 50,
      fontFamily: 'Roboto',
      fontWeight: 'bold'
    }
  }
});
