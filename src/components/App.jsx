import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Header from './ui/Header';
import constant from '../constant';
import Startseite from './Startseite';
import Aktuelles from './Aktuelles';
import Footer from './ui/Footer';

const App = () => {
  return (
    <>
      <Header />
      <Switch>
        <Route
          exact
          path={[`/${constant.root}`, '/']}
          component={() => <Startseite />}
        />
        <Route
          exact
          path={`/${constant.news}`}
          component={() => <Aktuelles />}
        />
        <Route
          exact
          path={`/${constant.open}`}
          component={() => <div>{constant.open}</div>}
        />
        <Route
          exact
          path={`/${constant.path}`}
          component={() => <div>{constant.path}</div>}
        />
        <Route
          exact
          path={`/${constant.connect}`}
          component={() => <div>{constant.connect}</div>}
        />
        <Route
          exact
          path={`/${constant.links}`}
          component={() => <div>{constant.links}</div>}
        />
      </Switch>
      <Footer />
    </>
  );
};

export default App;
