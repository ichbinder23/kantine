import React from 'react';

import Headline from './ui/Headline';
import headlineImg from '../assets/Aktuelles.jpg';

const Aktuelles = () => {
  return <Headline imgSrc={headlineImg} headlineText="Aktuelles" />;
};

export default Aktuelles;
