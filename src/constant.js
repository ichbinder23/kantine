const menuItems = {
  root: 'Startseite',
  news: 'Aktuelles',
  open: 'Öffnungszeiten',
  path: 'Anfahrt',
  connect: 'Kontakt',
  links: 'Handwerkerhof'
};

export default menuItems;

export const text = {
  article: {
    headline: 'Liebe Gäste',
    subheadline: 'Essen für die Seele',
    teaserText:
      'schön, dass sie uns entdeckt haben. Eingebettet zwischen der feldberger- Seenlandschaft und dem Uckermärkischen Naturpark liegt unser Dorf Thomsdorf. Wir hoffen, sie verbringen eine schöne zeit bei uns geniessen unser essen welches frisch für sie zubereitet wird.',
    text:
      'schön, dass sie uns entdeckt haben. Eingebettet zwischen der feldberger- Seenlandschaft und dem Uckermärkischen Naturpark liegt unser Dorf Thomsdorf. Wir hoffen, sie verbringen eine schöne zeit bei uns geniessen unser essen welches frisch für sie zubereitet wird. Dabei ist uns wichtig, dass die Zutaten aus der Region kommen. Unsere direkten Partner sind Z.B. die GÄrtnerei Hof im Winkel in Thomsdorf, die Schäferei Hullerbusch, Müritzwild, Biofleischmanufaktur velten, Bioeismanufaktur Templin und Terra-Naturkost Berlin. Unser sauerteigbrot backen wir selbst und bauen einen teil der Zutaten direkt auf dem acker hier in Thomsdorf an. Die Keramik, auf der wir unsere speisen servieren wird auf dem Kunsthandwerkerhof Thomsdorf in der keramikwerkstatt Anne Schattka-Steinbruch hergestellt. <br/> Wir hoffen sie schmecken die Frische und die freude die wir beim herstellen der Speisen empfinden.<br/> <br/>Unser Motto -ESSEN FÜR DIE SEELE- bedeutet Frieden für uns, <br/>für sie und für unsere Welt.<br/><br/>...GUTEN APPETIT!'
  },
  article2: {
    headline: 'Öffnungszeiten',
    subheadline: 'Unsere Öffnungszeiten',
    teaserText:
      'Unsere Öffnungszeiten sind vielfältig wie unsere Speisen, daher haben wir eine Informationsseite für sie erstellt auf der Sie einsehen können ob wir gerade offen haben und wenn unsere Feste stattfinden.',
    text: ''
  }
};
